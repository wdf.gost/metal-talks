//
//  Renderer00.m
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 25/2/21.
//

#import "Renderer02.h"
#import "MetalUtils.h"


static const NSUInteger MaxInstances = 256;



@implementation Renderer02
{
	id<MTLDevice> _device;
	id<MTLCommandQueue> _commandQueue;
	MTKView *_mtkView;
	
	id<MTLRenderPipelineState> _pipelineRender;
	id<MTLComputePipelineState> _pipelineCompute;
	id<MTLTexture> _texture;
	id<MTLBuffer> _bufferVertices;
	id<MTLBuffer> _bufferIndices;
	id<MTLBuffer> _bufferTransforms;
	id<MTLBuffer> _bufferPositions;
	
	NSUInteger _instances;
	
	CFTimeInterval _prevTime;
	FPSUpdater _fpsUpdater;
	double _prevFPS[MaxFPSHistory];
	NSUInteger _prevFPSNum;
	
	ImageMesh _mesh;
}



+ (nonnull instancetype)rendererWithMetalKitView:(nonnull MTKView *)view
									  fpsUpdater:(FPSUpdater _Nonnull )fpsUpdater
{
	return [[self.class alloc] initWithMetalKitView:view fpsUpdater:fpsUpdater];
}



-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view  fpsUpdater:(FPSUpdater _Nonnull )fpsUpdater
{
	self = [super init];
	if (self) {
		_fpsUpdater = fpsUpdater;
		_prevFPSNum = 0;
		
		_mtkView = view;
		_device = view.device;
		
		_commandQueue = [_device newCommandQueue];
		
		_instances = MaxInstances;
		
		MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor new];
		vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
		vertexDescriptor.attributes[0].offset = 0;
		vertexDescriptor.attributes[0].bufferIndex = 0;
		vertexDescriptor.attributes[1].format = MTLVertexFormatFloat2;
		vertexDescriptor.attributes[1].offset = offsetof(VertexData, texture);
		vertexDescriptor.attributes[1].bufferIndex = 0;
		vertexDescriptor.layouts[0].stride = sizeof(VertexData);
		vertexDescriptor.layouts[0].stepRate = 1;
		vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
		_pipelineRender = buildRenderPipeline(_device, _mtkView, @"fshRenderer02", @"vshRenderer02", vertexDescriptor);
		
		_pipelineCompute = buildComputePipeline(_device, @"krnRenderer02");
		
		_mesh = loadImageMesh(@"Cat1");
		_texture = loadTexture(_device, @"Cat1");
		
		_bufferVertices = [_device newBufferWithBytes:_mesh.vertices
											   length:sizeof(VertexData) * _mesh.vertices_count
											  options:MTLResourceStorageModeShared];
		_bufferIndices = [_device newBufferWithBytes:_mesh.indices
											  length:sizeof(UInt16) * _mesh.indices_count
											 options:MTLResourceStorageModeShared];
		_bufferTransforms = [_device newBufferWithLength:_instances * sizeof(simd_float4x4)
												 options:MTLResourceStorageModePrivate];
		
		PositionData positions[MaxInstances];
		[self fillPositions:positions count:MaxInstances];
		_bufferPositions = [_device newBufferWithBytes:positions
												length:sizeof(PositionData) * _instances
											   options:MTLResourceStorageModeShared];
	}
	
	return self;
}



- (void)dealloc {
	free(_mesh.indices);
	free(_mesh.vertices);
}



- (NSString *)demoDescription {
	NSLog(@"\nCase 02:\n"
		  @"\tCanvas size: %dx%d\n"
		  @"\tVertices per instance: %ld\n"
		  @"\tInstances: %ld",
		  (int)(self.mtkView.frame.size.width * self.mtkView.contentScaleFactor),
		  (int)(self.mtkView.frame.size.height * self.mtkView.contentScaleFactor),
		  _mesh.vertices_count,
		  _instances);
	return @"r02 - Update transforms on GPU";
}


- (MTKView *)mtkView {
	return _mtkView;
}



// MARK: - Metal



- (void)fillPositions:(PositionData *)positions count:(NSUInteger)count {
	for (NSUInteger i = 0; i < count; ++i) {
		positions[i] = (PositionData) {
			.translate = simd_make_float2(rand11(), rand11()),
			.velocity = 0.005 * simd_make_float2(rand11(), rand11()),
			.angle = rand01(),
			.scale = 0.1 + 0.8 * rand11(),
		};
	}
}



- (id<MTLTexture>)createTextureWithSize:(CGSize)size format:(MTLPixelFormat)format {
	MTLTextureDescriptor *textureDescriptor =
	[MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
													   width:size.width
													  height:size.height
												   mipmapped:NO];
	textureDescriptor.storageMode = MTLStorageModePrivate;
	textureDescriptor.usage = (MTLTextureUsageShaderRead |
							   MTLTextureUsageShaderWrite |
							   MTLTextureUsageRenderTarget);
	
	id<MTLTexture> res = [_device newTextureWithDescriptor:textureDescriptor];
	NSAssert(res != nil, @"Problems with creating new texture.");
	return res;
}



- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size
{
}



- (void)drawInMTKView:(nonnull MTKView *)view
{
	id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
	commandBuffer.label = @"Final draw";
	
	[self updateTransformsIn:commandBuffer];
	[self renderIn:commandBuffer];
	
	[commandBuffer presentDrawable:view.currentDrawable];
	[commandBuffer commit];
	
	[self updateFPS];
}



- (void)updateFPS {
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		_prevTime = CACurrentMediaTime() - 1.0 / 60.0;
	});
	
	CFTimeInterval curTime = CACurrentMediaTime();
	double fps = 1.0 / (curTime - _prevTime);
	
	double fpsSum = fps;
	int fpsCount = 1;
	for (NSUInteger i = 0; i < _prevFPSNum; ++i) {
		fpsSum += _prevFPS[i];
		++fpsCount;
	}
	for (int i = (int)_prevFPSNum - 1; i > 0; --i) {
		_prevFPS[i] = _prevFPS[i - 1];
	}
	_prevFPS[0] = fps;
	_prevFPSNum = MIN(_prevFPSNum + 1, MaxFPSHistory);
	_fpsUpdater(fpsSum / fpsCount);
	_prevTime = curTime;
}



- (void)updateTransformsIn:(id<MTLCommandBuffer>)commandBuffer {
	id<MTLComputeCommandEncoder> encoder = [commandBuffer computeCommandEncoder];
	encoder.label = @"Update transforms";
	[encoder setComputePipelineState:_pipelineCompute];
	
	[encoder setBuffer:_bufferPositions offset:0 atIndex:0];
	[encoder setBuffer:_bufferTransforms offset:0 atIndex:1];
	
	MTLSize threadgroupSize = MTLSizeMake(_instances / 16, 1, 1);
	MTLSize threadgroupCount = MTLSizeMake((_instances + threadgroupSize.width - 1) / threadgroupSize.width, 1, 1);
	[encoder dispatchThreadgroups:threadgroupCount threadsPerThreadgroup:threadgroupSize];
	
	[encoder endEncoding];
}



- (void)renderIn:(id<MTLCommandBuffer>)commandBuffer {
	MTLRenderPassDescriptor* renderPassDescriptor = self.mtkView.currentRenderPassDescriptor;
	renderPassDescriptor.colorAttachments[0].clearColor = (MTLClearColor){0, 0.2, 0.2, 1};
	renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
	
	if (renderPassDescriptor != nil) {
		id <MTLRenderCommandEncoder> encoder =
		[commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
		encoder.label = @"Render cats";
		
		[encoder setRenderPipelineState:_pipelineRender];
		[encoder setVertexBuffer:_bufferVertices offset:0 atIndex:0];
		[encoder setVertexBuffer:_bufferTransforms offset:0 atIndex:1];
		[encoder setFragmentTexture:_texture atIndex:0];
		
		[encoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
							indexCount:_mesh.indices_count
							 indexType:MTLIndexTypeUInt16
						   indexBuffer:_bufferIndices
					 indexBufferOffset:0
						 instanceCount:_instances];
		
		[encoder endEncoding];
	}
}

@end
