//
//  MetalIntroductionDemoApp.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 12/2/21.
//

import SwiftUI

@main
struct MetalIntroductionDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
