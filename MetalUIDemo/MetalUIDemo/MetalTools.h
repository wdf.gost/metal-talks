//
//  MetalTools.h
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>


NS_ASSUME_NONNULL_BEGIN

id<MTLRenderPipelineState> buildRenderPipeline(id<MTLDevice> device,
											   MTLPixelFormat format,
											   NSUInteger sampleCount,
											   NSString *fragment,
											   NSString *vertex);

id<MTLTexture> loadTexture(id<MTLDevice> device, UIImage *image);

id<MTLTexture> createTexture(id<MTLDevice> device,
							 int width,
							 int height,
							 MTLPixelFormat format);

id<MTLBuffer> buildMesh(id<MTLDevice> device,
						MTLSize size,
						MTLSize shift,
						int cellSize,
						NSUInteger *vertexCount);

NS_ASSUME_NONNULL_END
