//
//  compute.metal
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 13/2/21.
//

#include <metal_stdlib>
using namespace metal;


typedef struct
{
	float4 position [[position]];
	float pointSize [[point_size]];
	float4 color;
} ColorInOut;



vertex ColorInOut vshCompute(unsigned int vid [[vertex_id]],
							 constant float4 *pos_vel [[buffer(0)]])
{
	ColorInOut out;
	
	out.position = float4(pos_vel[vid].xy, 0.0, 1.0);
	
	out.pointSize = length(pos_vel[vid].zw) * 1024;
	
	float2 velocity = pos_vel[vid].zw * 128;
	float angle = cos(0.5 * atan2(velocity.x, velocity.y));
	out.color = abs(float4(angle, velocity.x, velocity.y, 1.0));
	
	return out;
}



fragment float4 fshCompute(ColorInOut in [[stage_in]],
						   float2 pointCoord  [[point_coord]])
{
	float2 inPointCoord = pointCoord - 0.5;
	if (length(inPointCoord) > 0.5) {
		discard_fragment();
	}
	return in.color;
}



inline static float2 rand2(float2 p) {
	p = float2(dot(p, float2(127.1, 311.7)),
			   dot(p, float2(269.5, 183.3)));
	return -1.0 + 2.0 * fract(sin(p) * 43758.5453123);
}



kernel void krnCompute(device float4 *pos_val [[buffer(0)]],
					   uint gid [[thread_position_in_grid]])
{
	float4 particle = pos_val[gid];
	
	if (any(abs(particle.xy) > 1)) {
		particle.xy = 0;//rand2(particle.xy + particle.zw) * 0.001;
	}
	if (length(particle.xy) < 1e-6) {
		particle.zw = float2(gid % 128, gid / 128) / 128 - 0.5;
		particle.zw += rand2(particle.xy + particle.zw);
		particle.zw *= 0.0001;
	}
	particle.xy += particle.zw;
	particle.zw *= 1.01;
	
	pos_val[gid] = particle;
}
