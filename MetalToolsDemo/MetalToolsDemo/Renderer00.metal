//
//  Renderer00.metal
//  MetalToolsDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



typedef struct {
	float4 position [[attribute(0)]];
	float2 texture [[attribute(1)]];
} VertexData;



static half3 rgb2hsv(half3 c) {
	constexpr half4 K = half4(0.0h, -1.0h / 3.0h, 2.0h / 3.0h, -1.0h);
	half4 p = select(half4(c.bg, K.wz), half4(c.gb, K.xy), c.b < c.g);
	half4 q = select(half4(p.xyw, c.r), half4(c.r, p.yzx), p.x < c.r);
	
	half d = q.x - min(q.w, q.y);
	half e = 6.0e-8h;
	return half3(abs(q.z + (q.w - q.y) / (6.0h * d + e)), d / (q.x + e), q.x);
}



template <typename T>
inline static T pos_fract(T x) {
	return x - floor(x);
}



static half3 hsv2rgb(half3 c) {
	constexpr half3 K = half3(1.0h, 2.0h / 3.0h, 1.0h / 3.0h);
	half3 p = abs(pos_fract(c.x + K) * 6.0h - 3.0h);
	return c.z * fma(clamp(p - 1.0h, 0.0h, 1.0h), c.y, 1.0h - c.y);
}



static half3 time_color(half2 uv, half time) {
	half2 uvl = uv * half2(3.0h, 2.0h);
	half2 tcs = time + half2(M_PI_2_H, 0.0h);
	half2 tsc = time + half2(0.0h, M_PI_2_H);
	
	half2 a = uvl + tcs;
	half2 a_cs = sin(a);
	half2 b = fma(uvl, a_cs, tsc);
	half2 b_sc = sin(b);
	
	return fma(abs(half3(b_sc, 0.4h / 0.9h)), 0.9h, 0.1h);
}



vertex ColorInOut vshRenderer00(VertexData in [[stage_in]],
								unsigned int iid [[instance_id]],
								device const float4x4 *transforms [[buffer(1)]],
								constant float &ratio [[buffer(2)]])
{
	ColorInOut out;
	out.position = transforms[iid] * in.position;
	out.position.y *= ratio;
	out.texCoord = in.texture;
	
	return out;
}



fragment half4 fshRenderer00(ColorInOut in [[stage_in]],
							 constant float &time_ [[buffer(0)]],
							 texture2d<half> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_zero, filter::nearest);
	
	float2 shift = in.texCoord - 0.5;
	float angle = atan2(shift.y, shift.x);
	
	half time = half(time_);
	half4 src = image.sample(imageSampler, in.texCoord);
	half3 hsv = rgb2hsv(src.rgb);
	half3 col = time_color(half2(in.texCoord), time);
	hsv.x = pos_fract(fma(half(angle), 0.5h / M_PI_H, 0.5h) - fma(time, 0.25h, -rgb2hsv(col).x));
	
	half k = smoothstep(0.0h, 1.0h, length(half2(shift)));
	hsv.y = fma(hsv.y, 1.0h - k, k);
	src.rgb = hsv2rgb(max(hsv, 0.0h));
	
	return src;
}



kernel void krnRenderer00(device PositionData *positions [[buffer(0)]],
						  device float4x4 *transforms [[buffer(1)]],
						  uint gid [[thread_position_in_grid]])
{
	PositionData pos = positions[gid];
	
	constexpr float pi2 = 1.57079632679;
	float2 cs = cos(float2(pos.angle, pos.angle - pi2));
	float s = pos.scale + 0.05 * sin(length(pos.translate));
	transforms[gid] =
	(
	 float4x4(float4(cs.x * s, cs.y, 0, 0),
			  float4(-cs.y, cs.x * s, 0, 0),
			  float4(0, 0, 1, 0),
			  float4(pos.translate.x, pos.translate.y, 0, 1))
	 );
	
	float m = -1.0 + pos.scale;
	float M = 1.0 - pos.scale;
	if (pos.translate.x < m || pos.translate.x > M) {
		pos.translate.x = clamp(pos.translate.x, m, M);
		pos.velocity.x = -pos.velocity.x;
	}
	if (pos.translate.y < m || pos.translate.y > M) {
		pos.translate.y = clamp(pos.translate.y, m, M);
		pos.velocity.y = -pos.velocity.y;
	}
	pos.translate += pos.velocity;
	pos.angle += length(pos.velocity) * pos.translate.x * pos.translate.y * 10.0;
	
	positions[gid] = pos;
}
