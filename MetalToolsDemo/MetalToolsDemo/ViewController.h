//
//  ViewController.h
//  MetalToolsDemo
//
//  Created by George Ostrobrod on 3/2/21.
//

#import <UIKit/UIKit.h>
#import <MetalKit/MetalKit.h>


@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet MTKView *mtkView;

@end

