import MetalKit
import Metal



class RendererSimpleQuad_Scissor: RendererSimple {
	
	var pipeline: MTLRenderPipelineState
	
	override init?(metalKitView: MTKView) {
		do {
			pipeline = try buildRenderPipeline(device: metalKitView.device!,
											   metalKitView: metalKitView,
											   fragment: "fragmentShader",
											   vertex: "vertexShader")
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		
		super.init(metalKitView: metalKitView)
	}
	
	
	override func description() -> String {
		return "Color Quad + Scissor"
	}
	
	
	
	// MARK: - Render
	override func render(in commandBuffer: MTLCommandBuffer) {
		let finalRPD = self.metalView.currentRenderPassDescriptor
		if let finalRPD = finalRPD {
			finalRPD.colorAttachments[0].clearColor = MTLClearColorMake(0,0.2,0.2,1)
			finalRPD.colorAttachments[0].loadAction = .clear
			if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: finalRPD) {
				renderEncoder.setScissorRect(MTLScissorRect(x: Int(size.width * 0.125),
															y: Int(size.height * 0.125),
															width: Int(size.width * 0.5),
															height: Int(size.width * 0.5)))
				
				renderEncoder.setRenderPipelineState(pipeline)
				renderEncoder.drawPrimitives(type: .triangleStrip, vertexStart: 0, vertexCount: 4)
				renderEncoder.endEncoding()
			}
		}
	}
	
}
