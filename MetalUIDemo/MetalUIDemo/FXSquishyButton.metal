//
//  FXButton.metal
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#include <metal_stdlib>
using namespace metal;


typedef struct {
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;




// MARK: -

float rand(float2 pos) {
	return fract(sin(dot(pos, float2(12.9898, 78.233))) * 43758.5453123);
}

vertex ColorInOut vshSquishyButton(unsigned int vid [[ vertex_id ]],
								   texture2d<float> dudvMap [[texture(0)]],
								   constant float4 *vertices [[buffer(0)]],
								   constant float2 &inv_size [[buffer(1)]],
								   constant float &crash_time [[buffer(2)]])
{
	constexpr sampler imageSampler(address::clamp_to_zero, filter::linear);
	float4 vert = vertices[vid];
	float2 cur_point = vert.xy * inv_size;
	float2 dudv = dudvMap.sample(imageSampler, cur_point).rg;
	dudv *= 0.25 + 0.75 * rand(vert.zw);
	float2 pos = (cur_point + dudv) * 2 - 1;
	pos.y = -pos.y;
	
//	if (length(dudv) > length(inv_size)) {
//		pos.y = mix(pos.y, -1, smoothstep(vert.z, vert.w, crash_time));
//	}
	
	ColorInOut out;
	
	out.position = float4(pos, 0, 1);
	out.texCoord = cur_point;
	
	return out;
}



fragment float4 fshSquishyButton(ColorInOut in [[stage_in]],
								 texture2d<float> image [[texture(0)]])
{
	constexpr sampler imageSampler(address::clamp_to_zero, filter::linear);
	return image.sample(imageSampler, in.texCoord);
}



// MARK: -

vertex ColorInOut vshDudvMapUpdate(unsigned int vid [[ vertex_id ]]) {
	constexpr float2 verts[] = {
		float2(0, 0),
		float2(0, 1),
		float2(1, 0),
		float2(1, 1)
	};
	
	float2 cur_point = verts[vid];
	float2 pos = cur_point * 2 - 1;
	pos.y = -pos.y;
	
	ColorInOut out;
	
	out.position = float4(pos, 0, 1);
	out.texCoord = cur_point;
	
	return out;
}



fragment float4 fshDudvMapUpdate(ColorInOut in [[stage_in]],
								 texture2d<float> image [[texture(0)]]) {
	constexpr sampler imageSampler(address::clamp_to_zero, filter::nearest);
	return image.sample(imageSampler, in.texCoord) * 0.95;
}



// MARK: -

vertex ColorInOut vshDudvMapDraw(unsigned int vid [[ vertex_id ]],
								 constant float2 &point [[buffer(0)]],
								 constant float2 &invSize [[buffer(1)]],
								 constant float &radius [[buffer(2)]])
{
	constexpr float2 verts[] = {
		float2(-0.5, -0.5),
		float2(-0.5, +0.5),
		float2(+0.5, -0.5),
		float2(+0.5, +0.5)
	};
	
	float2 cur_point = verts[vid] * radius;
	float2 pos = (cur_point + point) * invSize * 2 - 1;
	pos.y = -pos.y;
	
	ColorInOut out;
	
	out.position = float4(pos, 0, 1);
	out.texCoord = verts[vid];
	
	return out;
}



fragment float4 fshDudvMapDraw(ColorInOut in [[stage_in]],
							   constant float2 &invSizeRadius [[buffer(0)]])
{
	float2 dst = in.texCoord;
	dst *= 1.0 - smoothstep(0.0, 0.5, length(dst));
	dst *= invSizeRadius;
	
	return float4(dst, 0.0, 0.0);
}
