//
//  ViewController.m
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}



- (IBAction)onTouchUpInside:(UIButton *)sender {
	_image.hidden = !_image.hidden;
}

@end
