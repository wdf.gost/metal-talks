//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;


static float rand(float2 pos) {
	return fract(sin(dot(pos, float2(0.129898h, 0.78233h))) * 437.585453123h);
}



fragment float4 fshTexture00D(ColorInOut in [[stage_in]],
							  texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::repeat, filter::linear);
	
	float2 uv = in.texCoord;
	
	float k = 4;
	uv += rand(floor(uv * k));
	uv.x += k * sin(uv.y * k);
	
	return image.sample(imageSampler, uv);
}
