//
//  MetalUtils.m
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#import "MetalUtils.h"


ImageMesh loadImageMesh(NSString *name) {
	ImageMesh mesh = (ImageMesh) {
		.imageName = nil,
		.imageSize = simd_make_float2(0, 0),
		.vertices = NULL,
		.vertices_count = 0,
		.indices = NULL,
		.indices_count = 0,
	};
	NSURL *url = [NSBundle.mainBundle URLForResource:name withExtension:@"json" subdirectory:@"data"];
	NSData *data = [NSData dataWithContentsOfURL:url];
	NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
	if (json == nil) {
		return mesh;
	}
	
	mesh.imageName = json[@"file"];
	mesh.imageSize = simd_make_float2(((NSNumber *)json[@"size"][0]).intValue,
									  ((NSNumber *)json[@"size"][1]).intValue);
	
	NSArray *rawVertices = json[@"vertices"];
	NSArray *rawTriangles = json[@"triangles"];
	
	mesh.indices = malloc(rawTriangles.count * 3 * sizeof(UInt16));
	mesh.indices_count = rawTriangles.count * 3;
	
	mesh.vertices = malloc(rawVertices.count * sizeof(VertexData));
	mesh.vertices_count = rawVertices.count;
	
	for (int i = 0; i < rawTriangles.count; ++i) {
		mesh.indices[i * 3 + 0] = ((NSNumber *)rawTriangles[i][0]).intValue;
		mesh.indices[i * 3 + 1] = ((NSNumber *)rawTriangles[i][1]).intValue;
		mesh.indices[i * 3 + 2] = ((NSNumber *)rawTriangles[i][2]).intValue;
	}
	for (int i = 0; i < rawVertices.count; ++i) {
		float x = ((NSNumber *)rawVertices[i][0]).floatValue / mesh.imageSize.x;
		float y = ((NSNumber *)rawVertices[i][1]).floatValue / mesh.imageSize.y;
		
		mesh.vertices[i].texture = simd_make_float2(x, y);
		mesh.vertices[i].position = simd_make_float4(2 * x - 1, -(2 * y - 1), 0, 1);
	}
	
	return mesh;
}



id<MTLTexture> loadTexture(id<MTLDevice> device, NSString *imageName) OVERLOADED {
	NSString *path =  [NSBundle.mainBundle pathForResource:imageName ofType:@"png" inDirectory:@"data"];
	UIImage *image = [UIImage imageWithContentsOfFile:path];
	return loadTexture(device, image);
}



id<MTLTexture> loadTexture(id<MTLDevice> device, UIImage *image) OVERLOADED {
	MTKTextureLoader *loader = [[MTKTextureLoader alloc] initWithDevice:device];
	return [loader newTextureWithCGImage:image.CGImage
								 options:@{
									 MTKTextureLoaderOptionTextureUsage:
										 @(MTLTextureUsageShaderRead |
										 MTLTextureUsageShaderWrite |
										 MTLTextureUsageRenderTarget),
									 MTKTextureLoaderOptionTextureStorageMode:
										 @(MTLStorageModeShared),
									 MTKTextureLoaderOptionSRGB:
										 @(NO)
								 }
								   error:nil];
}



id<MTLRenderPipelineState> buildRenderPipeline(id<MTLDevice> device,
											   MTKView *metalKitView,
											   NSString *fragment,
											   NSString *vertex,
											   MTLVertexDescriptor *vertexDescriptor) OVERLOADED
{
	return buildRenderPipeline(device, metalKitView, fragment, vertex, vertexDescriptor, 1);
}



id<MTLRenderPipelineState> buildRenderPipeline(id<MTLDevice> device,
											   MTKView *metalKitView,
											   NSString *fragment,
											   NSString *vertex,
											   MTLVertexDescriptor *vertexDescriptor,
											   int attachments) OVERLOADED
{
	id<MTLLibrary> library = [device newDefaultLibrary];
	id<MTLFunction> vertexFunction = [library newFunctionWithName:vertex];
	id<MTLFunction> fragmentFunction = [library newFunctionWithName:fragment];
	
	MTLRenderPipelineDescriptor *descriptor = [MTLRenderPipelineDescriptor new];
	descriptor.label = [NSString stringWithFormat:@"%@ -> %@", vertex, fragment];
	descriptor.sampleCount = metalKitView.sampleCount;
	descriptor.vertexFunction = vertexFunction;
	descriptor.fragmentFunction = fragmentFunction;
	descriptor.vertexDescriptor = vertexDescriptor;
	
	for (int i = 0; i < attachments; ++i) {
		descriptor.colorAttachments[i].pixelFormat = metalKitView.colorPixelFormat;
	}
	descriptor.depthAttachmentPixelFormat = metalKitView.depthStencilPixelFormat;
	descriptor.stencilAttachmentPixelFormat = metalKitView.depthStencilPixelFormat;
	
	return [device newRenderPipelineStateWithDescriptor:descriptor error:nil];
}



id<MTLComputePipelineState> buildComputePipeline(id<MTLDevice> device, NSString *kernel) {
	id<MTLLibrary> library = [device newDefaultLibrary];
	id<MTLFunction> kernelFunction = [library newFunctionWithName:kernel];
	return [device newComputePipelineStateWithFunction:kernelFunction error:nil];
}



id<MTLTexture> createTexture(id<MTLDevice> device,
							 int width,
							 int height,
							 MTLPixelFormat format) OVERLOADED
{
	return createTexture(device, width, height, format, MTLStorageModePrivate);
}



id<MTLTexture> createTexture(id<MTLDevice> device,
							 int width,
							 int height,
							 MTLPixelFormat format,
							 MTLStorageMode storageMode) OVERLOADED
{
	MTLTextureDescriptor *descriptor = [MTLTextureDescriptor new];
	
	descriptor.pixelFormat = format;
	descriptor.width = width;
	descriptor.height = height;
	descriptor.storageMode = storageMode;
	descriptor.usage = (MTLTextureUsageShaderRead |
						MTLTextureUsageShaderWrite |
						MTLTextureUsageRenderTarget);
	
	return [device newTextureWithDescriptor:descriptor];
}
