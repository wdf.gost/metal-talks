//
//  SceneDelegate.h
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

