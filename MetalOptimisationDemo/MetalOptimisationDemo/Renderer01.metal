//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



typedef struct {
	float4 position [[attribute(0)]];
	float2 texture [[attribute(1)]];
} VertexData;



vertex ColorInOut vshRenderer01(VertexData in [[stage_in]],
								unsigned int iid [[instance_id]],
								device const float4x4 *transforms [[buffer(1)]])
{
	ColorInOut out;
	out.position = transforms[iid] * in.position;
	out.texCoord = in.texture;
	
	return out;
}



fragment float4 fshRenderer01(ColorInOut in [[stage_in]],
							  float4 back [[ color(0) ]],
							  texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_zero, filter::nearest);
	
	float4 res = image.sample(imageSampler, in.texCoord);
	
	// NOTE: Source image is unpremultiplied
	res.rgb = mix(back.rgb, res.rgb, res.a);
	
	return res;
}



kernel void krnRenderer01(device PositionData *positions [[buffer(0)]],
						  device float4x4 *transforms [[buffer(1)]],
						  uint gid [[thread_position_in_grid]])
{
	PositionData pos = positions[gid];
	
	constexpr float pi2 = 1.57079632679;
	float2 cs = cos(float2(pos.angle, pos.angle - pi2));
	float s = pos.scale + 0.05 * sin(length(pos.translate));
	transforms[gid] =
	(
	 float4x4(float4(cs.x * s, cs.y, 0, 0),
			  float4(-cs.y, cs.x * s, 0, 0),
			  float4(0, 0, 1, 0),
			  float4(pos.translate.x, pos.translate.y, 0, 1))
	 );
	
	float m = -1.0 + pos.scale;
	float M = 1.0 - pos.scale;
	if (pos.translate.x < m || pos.translate.x > M) {
		pos.translate.x = clamp(pos.translate.x, m, M);
		pos.velocity.x = -pos.velocity.x;
	}
	if (pos.translate.y < m || pos.translate.y > M) {
		pos.translate.y = clamp(pos.translate.y, m, M);
		pos.velocity.y = -pos.velocity.y;
	}
	pos.translate += pos.velocity;
	pos.angle += length(pos.velocity) * pos.translate.x * pos.translate.y * 10.0;
	
	positions[gid] = pos;
}
