//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;

vertex ColorInOut vshTexture00A(unsigned int vid [[vertex_id]])
{
	ColorInOut out;
	
	constexpr float2 vertices[] = {
		float2(0, 0),
		float2(1, 0),
		float2(0, 1),
		float2(1, 1)
	};
	
	float2 position = vertices[vid % 4];
	out.texCoord = position;
	out.position = float4(position * 2.0 - 1.0, 0.0, 1.0);
	out.position.y = -out.position.y;
	
	return out;
}



fragment float4 fshTexture00AFinal(ColorInOut in [[stage_in]],
								   texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_edge, filter::linear);
	return image.sample(imageSampler, in.texCoord);
}



static float rand(float2 pos) {
	return fract(sin(dot(pos, float2(0.129898h, 0.78233h))) * 437.585453123h);
}



fragment float4 fshTexture00A(ColorInOut in [[stage_in]],
							  texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::repeat, filter::linear);
	
	float2 uv = in.texCoord;
	
	float k = 0;
	uv += rand(floor(uv * k));
	uv.x += k * sin(uv.y * k);
	
	return image.sample(imageSampler, uv);
}
