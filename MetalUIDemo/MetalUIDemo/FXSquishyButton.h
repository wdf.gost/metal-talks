//
//  FXSquishyButton.h
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface FXSquishyButton : UIButton

@end

NS_ASSUME_NONNULL_END
