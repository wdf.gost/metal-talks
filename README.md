Demo code for 
- [Introduction to Metal](https://mobius-piter.ru/2021/spb/talks/1i4j6iddofqr3sws0scptk/) ([video](https://youtu.be/ZseYhJb8vAM))
- [Optimisation Metal](https://mobius-piter.ru/2021/spb/talks/4q1xpismd18pbky5u4mvkg/) ([video](https://youtu.be/KqWxwhL3hpo))
- [Apple Metal Tools](https://mobius-moscow.ru/talks/tools-and-tricks-for-working-with-apple-metal/) ([video](https://youtu.be/cZqtKG0enIk))

**DISCLAIMER:**
This code is only for demonstration purposes. From point of view of clean code, this code, in general, is dirty, but it's necessary for to show how Metal works in insolated clean contexts - that's why there is so many "copy-paste" code, etc.
