//
//  MetalUtils.h
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#ifndef MetalUtils_h
#define MetalUtils_h

#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import <Foundation/Foundation.h>

#import "RendererTypes.h"



#define OVERLOADED __attribute__((__overloadable__))

static const NSUInteger MaxFPSHistory = 64;



typedef struct {
	simd_float4 position;
	simd_float2 texture;
} VertexData;



typedef struct  {
	NSString *imageName;
	simd_float2 imageSize;
	VertexData *vertices;
	size_t vertices_count;
	UInt16 *indices;
	size_t indices_count;
} ImageMesh;



inline static float rand01() {
	return (rand() % 1025) / 1024.0;
}

inline static float rand11() {
	return (rand() % 2049) / 1024.0 - 1.0;
}



ImageMesh loadImageMesh(NSString *name);

id<MTLTexture> loadTexture(id<MTLDevice> device, NSString *imageName) OVERLOADED;
id<MTLTexture> loadTexture(id<MTLDevice> device, UIImage *image) OVERLOADED;
id<MTLTexture> createTexture(id<MTLDevice> device,
							 int width,
							 int height,
							 MTLPixelFormat format) OVERLOADED;
id<MTLTexture> createTexture(id<MTLDevice> device,
							 int width,
							 int height,
							 MTLPixelFormat format,
							 MTLStorageMode storageMode) OVERLOADED;

id<MTLRenderPipelineState> buildRenderPipeline(id<MTLDevice> device,
											   MTKView *metalKitView,
											   NSString *fragment,
											   NSString *vertex,
											   MTLVertexDescriptor *vertexDescriptor) OVERLOADED;
id<MTLRenderPipelineState> buildRenderPipeline(id<MTLDevice> device,
											   MTKView *metalKitView,
											   NSString *fragment,
											   NSString *vertex,
											   MTLVertexDescriptor *vertexDescriptor,
											   int attachments) OVERLOADED;
id<MTLComputePipelineState> buildComputePipeline(id<MTLDevice> device, NSString *kernel);



#endif /* MetalUtils_h */
