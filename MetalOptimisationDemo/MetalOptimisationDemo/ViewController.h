//
//  ViewController.h
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 3/2/21.
//

#import <UIKit/UIKit.h>
#import <MetalKit/MetalKit.h>


@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet MTKView *mtkView;
@property (weak, nonatomic) IBOutlet UILabel *demoName;
@property (weak, nonatomic) IBOutlet UILabel *estimatedFPS;



- (IBAction)onPrevDemo:(id)sender;
- (IBAction)onNextDemo:(id)sender;

@end

