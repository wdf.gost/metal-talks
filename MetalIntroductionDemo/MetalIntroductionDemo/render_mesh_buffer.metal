//
//  render_mesh.metal
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 16/2/21.
//

#include <metal_stdlib>
using namespace metal;


typedef struct
{
	float4 position [[position]];
} ColorInOut;


typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} TextureInOut;



typedef struct
{
	float2 position [[attribute(0)]];
} Vertex;




vertex ColorInOut vshMeshBuffers(unsigned int vid [[vertex_id]],
								 constant float2 *coords [[buffer(0)]],
								 constant ushort *indices [[buffer(1)]],
								 constant float2 &image_size [[buffer(2)]])
{
	ColorInOut out;
	
	float2 position = coords[indices[vid]] / image_size;
	out.position = float4(position * 2.0 - 1.0, 0.0, 1.0);
	out.position.y = -out.position.y;
	
	return out;
}



vertex ColorInOut vshMeshAttributes(Vertex in [[stage_in]],
									constant float2 &image_size [[buffer(2)]])
{
	ColorInOut out;
	
	float2 position = in.position / image_size;
	out.position = float4(position * 2.0 - 1.0, 0.0, 1.0);
	out.position.y = -out.position.y;
	
	return out;
}



vertex TextureInOut vshMeshTexture(Vertex in [[stage_in]],
								   constant float2 &image_size [[buffer(2)]])
{
	TextureInOut out;
	
	float2 position = in.position / image_size;
	out.position = float4(position * 2.0 - 1.0, 0.0, 1.0);
	
	// Mesh is flipped
	out.position.y = -out.position.y;
	
	out.texCoord = position;
	
	return out;
}



fragment float4 fshMeshColor(constant float4 &color [[buffer(0)]])
{
	return color;
}



fragment float4 fshMeshTexture(TextureInOut in [[stage_in]],
							   float4 back [[ color(0) ]],
							   texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_edge, filter::linear);
	
	float4 res = image.sample(imageSampler, in.texCoord);
	
	// NOTE: Source image is unpremultiplied
	res.rgb = mix(back.rgb, res.rgb, res.a);
	
	return res;
}



fragment float4 fshMeshTexturePremultiply(TextureInOut in [[stage_in]],
										  float4 back [[ color(0) ]],
										  texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_edge, filter::linear);
	
	float4 res = image.sample(imageSampler, in.texCoord);
	
	// NOTE: Blend in linear
	res *= res;
	back *= back;
	
	res += (1.0 - res.a) * back;
	
	return sqrt(res);
}
