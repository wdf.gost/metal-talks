//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



vertex ColorInOut vshRenderer05(unsigned int vid [[vertex_id]],
								constant int &cur_instance[[buffer(0)]],
								device const float4x4 *transforms [[buffer(1)]])
{
	ColorInOut out;
	
	constexpr float2 vertices[] = {
		float2(0, 0),
		float2(1, 0),
		float2(0, 1),
		float2(1, 1)
	};
	
	float2 position = vertices[vid % 4];
	out.texCoord = position;
	out.position = transforms[cur_instance] * float4(position * 2.0 - 1.0, 0.0, 1.0);
	
	return out;
}



fragment float4 fshRenderer05(ColorInOut in [[stage_in]],
							  float4 back [[ color(0) ]],
							  texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_zero, filter::linear);
	
	float4 res = image.sample(imageSampler, in.texCoord);
	
	// NOTE: Source image is unpremultiplied
	res.rgb = mix(back.rgb, res.rgb, res.a);
	
	return res;
}
