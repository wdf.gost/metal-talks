//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



kernel void krnCompute01(texture2d<float, access::read> in [[ texture(0) ]],
						 texture2d<float, access::write> out [[ texture(1) ]],
						 uint2 gid [[thread_position_in_grid]])
{
	float4 src = in.read(gid);
	out.write(src * src, gid);
}
