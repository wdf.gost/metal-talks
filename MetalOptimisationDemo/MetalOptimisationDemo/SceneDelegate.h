//
//  SceneDelegate.h
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 3/2/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

