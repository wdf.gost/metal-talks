//
//  RendererMesh.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 16/2/21.
//

import MetalKit
import Metal



class Renderer_Compute: RendererSimple {
	
	var renderPipeline: MTLRenderPipelineState
	var computePipeline: MTLComputePipelineState
	var vertices: MTLBuffer
	var instances: Int = 16384
	
	override init?(metalKitView: MTKView) {
		do {
			renderPipeline = try buildRenderPipeline(device: metalKitView.device!,
													 metalKitView: metalKitView,
													 fragment: "fshCompute",
													 vertex: "vshCompute")
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		do {
			computePipeline = try buildComputePipeline(device: metalKitView.device!,
													   kernel: "krnCompute")
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		
		if let v = metalKitView.device!.makeBuffer(length: instances * MemoryLayout<SIMD4<Float32>>.size,
												   options: .storageModePrivate)
		{
			vertices = v
		} else {
			return nil
		}
		
		super.init(metalKitView: metalKitView)
	}
	
	
	override func description() -> String {
		return "Compute + Render"
	}
	
	
	
	// MARK: - Render
	override func render(in commandBuffer: MTLCommandBuffer) {
		if let computeEncoder = commandBuffer.makeComputeCommandEncoder() {
			computeEncoder.setComputePipelineState(computePipeline)
			computeEncoder.setBuffer(vertices, offset: 0, index: 0)
			
			assert(instances % 16 == 0)
			
			let threadgroupSize = MTLSizeMake(instances / 16, 1, 1);
			let threadgroupCount = MTLSizeMake((instances + threadgroupSize.width - 1) / threadgroupSize.width, 1, 1);
			computeEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadgroupSize)
			computeEncoder.endEncoding()
		}
		
		let finalRPD = self.metalView.currentRenderPassDescriptor
		if let finalRPD = finalRPD {
			finalRPD.colorAttachments[0].clearColor = MTLClearColorMake(0,0.2,0.2,1)
			finalRPD.colorAttachments[0].loadAction = .clear
			if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: finalRPD) {
				renderEncoder.setRenderPipelineState(renderPipeline)				
				renderEncoder.setVertexBuffer(vertices, offset: 0, index: 0)
				renderEncoder.drawPrimitives(type: .point, vertexStart: 0, vertexCount: instances)
				renderEncoder.endEncoding()
			}
		}
	}
	
}
