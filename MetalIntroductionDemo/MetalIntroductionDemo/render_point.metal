//
//  render.metal
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 13/2/21.
//

#include <metal_stdlib>
using namespace metal;


typedef struct
{
	float4 position [[position]];
	float pointSize [[point_size]];
	float2 texCoord;
} ColorInOut;



vertex ColorInOut vshPoint(unsigned int vid [[vertex_id]])
{
	ColorInOut out;
	
	constexpr float2 vertices[] = {
		float2(0, 0),
		float2(1, 0),
		float2(0, 1),
		float2(1, 1)
	};
	
	float2 position = vertices[vid % 4];
	
	out.pointSize = 64;
	out.texCoord = position;
	
	out.position = float4(position - 0.5, 0.0, 1.0);
	
	return out;
}



fragment float4 fshPoint(ColorInOut in [[stage_in]],
						 float2 pointCoord  [[point_coord]])
{
	float2 inPointCoord = pointCoord - 0.5;
	if (length(inPointCoord) > 0.5) {
		discard_fragment();
	}
	return float4(in.texCoord, 0, 1);
}
