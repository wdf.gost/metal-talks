//
//  MetalUtils.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 13/2/21.
//

import Foundation
import MetalKit
import Metal



struct ImageMesh {
	var imageName: String;
	var imageSize: SIMD2<Float32>
	var vertices: [SIMD2<Float32>]
	var indices: [ushort]
}



func loadImageMesh(_ name: String) -> ImageMesh? {
	if let url = Bundle.main.url(forResource: name, withExtension: "json", subdirectory: "data") {
		do {
			if let data = try String(contentsOf: url).data(using: .utf8) {
				do {
					if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
						let imageName: String = json["file"] as! String
						let imageSize: [Int] = json["size"] as! [Int]
						let vertices: [[Float]] = json["vertices"] as! [[Float]]
						let triangles: [[Int]] = json["triangles"] as! [[Int]]
						var indices: [ushort] = []
						for t in triangles {
							indices.append(ushort(t[0]))
							indices.append(ushort(t[1]))
							indices.append(ushort(t[2]))
						}
						
						return ImageMesh(imageName: imageName,
										 imageSize: SIMD2<Float32>(Float32(imageSize[0]), Float32(imageSize[1])),
										 vertices: vertices.map {SIMD2<Float32>(Float32($0[0]),
																				Float32($0[1]))},
										 indices: indices)
					}
				} catch {
					print(error.localizedDescription)
				}
			}
		} catch {
			print(error.localizedDescription)
		}
	}
	return nil
}



func loadTexture(device: MTLDevice, imageName: String) -> MTLTexture? {
	if let path = Bundle.main.path(forResource: imageName, ofType: "png", inDirectory: "data") {
		if let image = UIImage(contentsOfFile: path) {
			return loadTexture(device: device, image: image)
		}
	}
	return nil
}



func loadTexture(device: MTLDevice, image: UIImage) -> MTLTexture? {
	let textureLoader = MTKTextureLoader(device: device)
	let textureLoaderOptions = [
		MTKTextureLoader.Option.textureUsage: NSNumber(value: MTLTextureUsage.shaderRead.rawValue),
		MTKTextureLoader.Option.textureStorageMode: NSNumber(value: MTLStorageMode.`private`.rawValue),
		MTKTextureLoader.Option.SRGB: false
	]
	
	var texture: MTLTexture? = nil
	do {
		texture = try textureLoader.newTexture(cgImage: image.cgImage!, options: textureLoaderOptions)
	} catch {
		print("Unable to load texture. Error info: \(error)")
	}
	
	return texture
}



func buildRenderPipeline(device: MTLDevice,
						 metalKitView: MTKView,
						 fragment: String,
						 vertex: String,
						 vertexDescriptor: MTLVertexDescriptor? = nil)
throws -> MTLRenderPipelineState
{
	/// Build a render state pipeline object
	
	let library = device.makeDefaultLibrary()
	
	let vertexFunction = library?.makeFunction(name: vertex)
	let fragmentFunction = library?.makeFunction(name: fragment)
	
	let pipelineDescriptor = MTLRenderPipelineDescriptor()
	pipelineDescriptor.label = "ScreenRenderPipeline"
	pipelineDescriptor.sampleCount = metalKitView.sampleCount
	pipelineDescriptor.vertexFunction = vertexFunction
	pipelineDescriptor.fragmentFunction = fragmentFunction
	if let vertexDescriptor = vertexDescriptor {
		pipelineDescriptor.vertexDescriptor = vertexDescriptor
	}
	
	pipelineDescriptor.colorAttachments[0].pixelFormat = metalKitView.colorPixelFormat
	pipelineDescriptor.depthAttachmentPixelFormat = metalKitView.depthStencilPixelFormat
	pipelineDescriptor.stencilAttachmentPixelFormat = metalKitView.depthStencilPixelFormat
	
	return try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
}



func buildComputePipeline(device: MTLDevice, kernel: String) throws -> MTLComputePipelineState {
	let library = device.makeDefaultLibrary()
	let kernelFunction = library?.makeFunction(name: kernel)
	return try device.makeComputePipelineState(function: kernelFunction!)
}



func createTexture(device: MTLDevice,
				   width: Int,
				   height: Int,
				   format: MTLPixelFormat)
-> MTLTexture
{
	let mtlTextureDescriptor = MTLTextureDescriptor();
	
	mtlTextureDescriptor.pixelFormat = format;
	mtlTextureDescriptor.width = width;
	mtlTextureDescriptor.height = height;
	mtlTextureDescriptor.storageMode = MTLStorageMode.private;
	mtlTextureDescriptor.usage = [
		MTLTextureUsage.shaderRead,
		MTLTextureUsage.shaderWrite,
		MTLTextureUsage.renderTarget]
	
	return device.makeTexture(descriptor: mtlTextureDescriptor)!
}
